#include<stdio.h>
#include<math.h>
float input1(float x2, float x1)
{
    return x2-x1;  
}
float input2(float y2, float y1)
{
    return y2-y1;  
}
float distance(float x2, float x1, float y2, float y1)
{
    return (sqrt(input1(x2,x1)*input1(x2,x1) + input2(y2,y1)*input2(y2,y1)));
}
int main()
{
    float x1,y1,x2,y2,d;
    printf("Enter coordinates of point1: \n");
    scanf("%f%f",&x1,&y1);
    printf("Enter coordinates of point2: \n");
    scanf("%f%f",&x2,&y2);
printf("The distance between (%.2f,%.2f) and (%.2f,%.2f) is %.2f\n",x1,y1,x2,y2,distance(x2,x1,y2,y1));
    return 0 ;
}